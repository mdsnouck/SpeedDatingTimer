# Speeddate Timer

###### A small guide for the Speeddate Timer   

This is a node app so you need to have Node.js installed.   

Run `npm install` once to install dependencies.    
Start the app with `npm start`.

Take a look at `public/js/timer.js` if you want to change colors or timing.

And I also need to mention that I used the Grayscale template by **David Miller**, Managing Parter at [Iron Summit Media Strategies](http://www.ironsummitmedia.com/) for this app.
* https://github.com/davidtmiller

An example is deployed online by [heroku](https://speeddatingtimer.herokuapp.com/).

You are not allowed to use the heart shaped logos or "Speeddate" lettering without my permission.

Have fun.   
-- Maarten Desnouck, 14 Feb 2016   
