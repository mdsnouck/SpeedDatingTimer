var express = require('express');
var router = express.Router();

/* GET smswall page. */
router.get('/', function(req, res, next) {
    res.render('timer', {
        title: 'Speeddate Timer' ,
    });
});

module.exports = router;
